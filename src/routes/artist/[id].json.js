import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const ArtistQuery = gql`
query Artist($id: String!) {
  artist(id: $id) {
    id
    name
    coverUrl
    genres {
      id
      name
    }
    albums {
      id
      name
      coverUrl
    }
    songs {
      id
      title
      duration
      coverUrl
    }   
  }
}
`;
export async function get(req, res, next) {
  try {
    const { id } = req.params;
    var result = await graphQLClient.query({
      query: ArtistQuery,
      variables: {
        id,
      },
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    // res.writeHead(200, {
    //   'Content-Type': 'application/json'
    // });
    res.end(JSON.stringify(result.data.artist));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}