import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const ArtistsQuery = gql`
query Artists {
  artists {
    id
    name
    coverUrl
    albums {
      id
      name
      coverUrl
    }
  }
}
`;
export async function get(req, res) {
  try {
    var result = await graphQLClient.request({
      query: ArtistsQuery,
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    res.end(JSON.stringify(result.artists));
  } catch (error) {
    console.log(error);
    res.end(JSON.stringify({ error: error.message }));
  }
}