import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const PlaylistQuery = gql`
query Playlist($id: String!) {
    playlist(id: $id) {
      id
      name
      coverUrl
      createdAt
      updatedAt
      songs {
        id
        title
        duration
        coverUrl
        signedUrl {
          signedUrl
        }
        album {
          id
          name
          coverUrl
        }
        artist {
          id
          name
        }
      }
    }
}
`;
export async function post(req, res) {
  try {
    const { id, playNow } = req.body;
    var result = await graphQLClient.query({
      query: PlaylistQuery, variables: { id }, context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    if (playNow === true) {
      req.session.currentIndex = 0;
      req.session.songs = result.data.playlist.songs;
      req.session.playlistId = result.data.playlist.Id;
      req.session.playlistName = result.data.playlist.name;
    }
    res.end(JSON.stringify(result.data.playlist));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}