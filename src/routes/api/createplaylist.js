import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const CreatePlaylist = gql`
mutation CreatePlaylist($name: String!, $ids: [String!]) {
    createplaylist(input: {collectionName: $name, ids: $ids}) {
        id
        name
        songs {
          id
          duration
          title
          coverUrl
          signedUrl {
            signedUrl
          }
          album {
            id
            name
            coverUrl
          }
          artist {
            id
            name
          }
        }
    }
}
`;
export async function post(req, res) {
  try {
    const { name, ids } = req.body;
    var result = await graphQLClient.mutate({
      mutation: CreatePlaylist, variables: {
        name,
        ids
      },
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    req.session.currentIndex = 0;
    req.session.playlistId = result.data.createplaylist.id;
    req.session.playlistName = result.data.createplaylist.name;
    req.session.songs = result.data.createplaylist.songs;
    res.end(JSON.stringify(result.data.createplaylist));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}