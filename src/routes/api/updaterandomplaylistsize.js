

export async function post(req, res) {
    try {
        const { randomplaylistsize } = req.body;
        if (req.session.settings !== undefined) {
            req.session.settings.randomplaylistsize = randomplaylistsize;
        } else {
            req.session.settings = {
                randomplaylistsize
            }
        }
        res.end(JSON.stringify({ ok: true }));
    } catch (error) {
        res.end(JSON.stringify({ error: error.message }));
    }
}