import {
    gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";


const SongQuery = gql`
query Song($id: String!) {
    song(id: $id) {
    id
    title
    duration
    coverUrl
    signedUrl {
      signedUrl
    }
    artist {
        id
        name
    }
    album {
        id
        name
    }
  }
}
`;
export async function post(req, res) {
    try {
        const { id, playNow } = req.body;
        var result = await graphQLClient.query({
            query: SongQuery, variables: { id }, context: {
                headers: {
                    Authorization: req.session.token,
                }
            }
        });
        if (!req.session.songs || playNow === true) {
            req.session.currentIndex = 0;
            req.session.playlistId = undefined;
            req.session.songs = [result.data.song];
        } else {
            req.session.songs = [...req.session.songs, result.data.song];
        }
        res.end(JSON.stringify(result.data.song));
    } catch (error) {
        res.end(JSON.stringify({ error: error.message }));
    }
}