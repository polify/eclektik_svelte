

export async function post(req, res) {
    try {
        const { currentIndex } = req.body;
        req.session.currentIndex = currentIndex;
        res.end(JSON.stringify({ ok: true }));
    } catch (error) {
        res.end(JSON.stringify({ error: error.message }));
    }
}