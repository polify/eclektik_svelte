export async function post(req, res) {
    try {
        const { album, playNow } = req.body;
        if (!req.session.songs || playNow === true) {
            req.session.currentIndex = 0;
            req.session.playlistId = undefined;
            req.session.playlistName = album.name;
            req.session.songs = album.songs;
        } else if (req.session.songs) {
            req.session.songs = [...req.session.songs, ...album.songs];
        }
        res.end(JSON.stringify({ ok: true }));
    } catch (error) {
        res.end(JSON.stringify({ error: error.message }));
    }
}