import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const CreateSharingRequestMutation = gql`
mutation CreateSharingRequest($toUserID: String!) {
  createsharingrequest(toUserID: $toUserID) {
    id
    to {
      id
    }
  }
}
`;
export async function post(req, res) {
  try {
    const { toUserID } = req.body;
    var result = await graphQLClient.mutate({
      mutation: CreateSharingRequestMutation,
      variables: { toUserID },
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    res.end(JSON.stringify(result.data.createsharingrequest));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}