import {
    gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const DeleteDeviceMutation = gql`
mutation DeleteDevice($id: String!) {
    deletedevice(id: $id)
}
`;
export async function post(req, res) {
    try {
        const { id } = req.body;
        var result = await graphQLClient.mutate({
            mutation: DeleteDeviceMutation,
            variables: { id },
            context: {
                headers: {
                    Authorization: req.session.token,
                }
            }
        });
        res.end(JSON.stringify(result.data.deletedevice));
    } catch (error) {
        res.end(JSON.stringify({ error: error.message }));
    }
}