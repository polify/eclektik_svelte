import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const PlaylistsQuery = gql`
query Playlists {
    playlists {
      id
      name
      createdAt
      updatedAt
      songs {
        id
        title
      }
    }
}
`;
export async function post(req, res) {
  try {
    var result = await graphQLClient.query({
      query: PlaylistsQuery,
      // fetchPolicy: '',
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    res.end(JSON.stringify(result.data.playlists));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}