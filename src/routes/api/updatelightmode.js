

export async function post(req, res) {
    try {
        const { lightmode } = req.body;
        if (req.session.settings !== undefined) {
            req.session.settings.lightmode = lightmode;
        } else {
            req.session.settings = {
                lightmode
            }
        }
        res.end(JSON.stringify({ ok: true }));
    } catch (error) {
        res.end(JSON.stringify({ error: error.message }));
    }
}