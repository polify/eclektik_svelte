import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const SearchQuery = gql`
query Search($query: String!) {
    search(query: $query) {
    total
    albums {
      total
      albums {
        id
        name
        coverUrl
      }
    }
    artists {
      total
      artists {
        id
        name
        coverUrl
      }
    }
    songs {
      total
      songs {
        id
        title
        coverUrl
      }
    }
  }
}
`;
export async function post(req, res) {
  try {
    const { query } = req.body;
    var result = await graphQLClient.query({
      query: SearchQuery, variables: {
        query
      },
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    res.end(JSON.stringify(result.data.search));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}