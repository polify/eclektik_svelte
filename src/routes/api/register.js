import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const RegisterMutation = gql`
mutation Register($username: String!, $password: String!, $inviteCode: String!) {
  register(input: {name: $username, password: $password}, inviteCode: $inviteCode) {
    id
    name
  }
}
`;
export async function post(req, res) {
  try {
    const { username, password, inviteCode } = req.body;
    var result = await graphQLClient.mutate({ mutation: RegisterMutation, variables: { username, password, inviteCode } },);
    res.end(JSON.stringify(result.data.register));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}