import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const UpdateSharingRequestMutation = gql`
mutation UpdateSharingRequest($id: String!, $accept: Boolean!, $refuse: Boolean!) {
  updatesharingrequest(id: $id, accept: $accept, refuse: $refuse) {
    id
  }
}
`

export async function post(req, res) {
  try {
    const { id, accept, refuse } = req.body;
    var result = await graphQLClient.mutate({
      mutation: UpdateSharingRequestMutation, variables: { id, accept, refuse }, context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    res.end(JSON.stringify(result.data.updatesharingrequest))
  } catch (e) {
    res.end(JSON.stringify({ error: e.message }));
  }
}