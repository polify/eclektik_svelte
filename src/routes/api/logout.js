import graphQLClient from "../../graphql_client";

export async function post(req, res) {
    try {
        await graphQLClient.clearStore();
        req.session.destroy();
        req.session.settings = {};
        res.end(JSON.stringify({ ok: true }));
    } catch (error) {
        res.end(JSON.stringify({ error: error.message }));
    }
}