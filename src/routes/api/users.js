import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const UsersQuery = gql`
query Users {
users {
    total
    users {
      id
      name
      createdAt
      updatedAt
      songsCount
      albumsCount
      playlistsCount
    }
  }
}
`;
export async function post(req, res) {
  try {
    var result = await graphQLClient.query({
      query: UsersQuery,
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    res.end(JSON.stringify(result.data.users));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}