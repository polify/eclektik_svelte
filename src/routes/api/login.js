import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const LoginQuery = gql`
query Login($username: String!, $password: String!) {
  login(name: $username, password: $password) {
    accessToken
    user {
      name
      id
    }
  }
}
`;
export async function post(req, res) {
  try {
    const { username, password } = req.body;

    var result = await graphQLClient.query({
      query: LoginQuery,
      variables: { username, password },
      fetchPolicy: "network-only"
    });

    if (!result.data.login && !result.data.login.accessToken) {
      throw Error("invalid data")
    }
    if (!req.session) req.session = {};
    req.session.token = result.data.login.accessToken;
    req.session.user = result.data.login.user;
    res.end(JSON.stringify({ token: result.data.login.accessToken, user: result.data.login.user }));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}