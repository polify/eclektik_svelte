import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const ProfileQuery = gql`
query Profile {
profile {
    id
    name
    createdAt
    updatedAt
    s3Credential {
      id
      bucketName
      bucketHost
    }
    devices {
      id
      name
      userAgent
      type
      lastIP
      lastConnection
      createdAt
      updatedAt
    }
    s3SharedCredential {
      id
      bucketName
      bucketHost
    }
    askedSharingRequests {
      id
      accepted
      refused
      createdAt
      updatedAt
      from {
        id
        name
      }
      to {
        id
        name
      }
    }
    receivedSharingRequests {
      id
      accepted
      refused
      createdAt
      updatedAt
      to {
        id
        name
      }
      from {
        id
        name
      }
    }
  }
}
`;
export async function post(req, res) {
  try {
    var result = await graphQLClient.query({
      query: ProfileQuery, context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    res.end(JSON.stringify(result.data.profile));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}