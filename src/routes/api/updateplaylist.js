import {
    gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const UpdatePlaylist = gql`
  mutation UpdatePlaylist($id: String!, $name: String!, $ids: [String!]) {
      updateplaylist(id: $id, input: {collectionName: $name, ids: $ids}) {
          id
          name
      }
  }
  `;
export async function post(req, res) {
    try {
        const { id, name, ids } = req.body;
        var result = await graphQLClient.mutate({
            mutation: UpdatePlaylist, variables: {
                id,
                name,
                ids
            },
            context: {
                headers: {
                    Authorization: req.session.token,
                }
            }
        });
        res.end(JSON.stringify(result.data.updateplaylist));
    } catch (error) {
        res.end(JSON.stringify({ error: error.message }));
    }
}