import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const LastUploadedSongsQuery = gql`
query LastUploadedSongs($limit: Int!) {
    lastuploadedSongs(pagination: {count: $limit}) {
    total
    songs {
      id
      title
      duration
      coverUrl
			album {
        id
        name
      }
      artist {
        id
        name
      }
    }
  }
}
`;
export async function post(req, res) {
  try {
    var { limit } = req.body;
    var result = await graphQLClient.query({
      query: LastUploadedSongsQuery,
      variables: { limit },
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    res.end(JSON.stringify(result.data.lastuploadedSongs));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}