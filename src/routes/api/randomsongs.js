import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const RandomPlaylist = gql`
query RandomPlaylist($limit: Int!) {
randomPlaylist(pagination: {count: $limit}) {
    id
    name
    songs {
      id
      title
      duration
      album {
        id
        name
        coverUrl
      }
      artist {
        id
        name
        coverUrl
      }
      signedUrl {
        signedUrl
      }
    }
  }
}
`;
export async function post(req, res) {
  try {
    const { total, playNow } = req.body;
    var result = await graphQLClient.query({
      query: RandomPlaylist,
      variables: { limit: total, },
      fetchPolicy: "no-cache",
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    if (playNow) {
      req.session.currentIndex = 0;
      req.session.playlistId = undefined;
      req.session.songs = result.data.randomPlaylist.songs;
      req.session.playlistName = result.data.randomPlaylist.name;
    }
    res.end(JSON.stringify(result.data.randomPlaylist));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}