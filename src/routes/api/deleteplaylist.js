import {
    gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const DeletePlaylistMutation = gql`
mutation DeleteDevice($id: String!) {
    deleteplaylist(id: $id)
}
`;
export async function post(req, res) {
    try {
        const { id } = req.body;
        var result = await graphQLClient.mutate({
            mutation: DeletePlaylistMutation,
            variables: { id },
            context: {
                headers: {
                    Authorization: req.session.token,
                }
            }
        });
        res.end(JSON.stringify(result.data.deleteplaylist));
    } catch (error) {
        res.end(JSON.stringify({ error: error.message }));
    }
}