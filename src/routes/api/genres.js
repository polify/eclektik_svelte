import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const GenresQuery = gql`
query Genres {
  genres(withsongs: true) {
    total
    genres {
      id
      name
      type
      
    }
  }
}
`;
export async function post(req, res) {
  try {
    var result = await graphQLClient.query({
      query: GenresQuery,
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    }, {}, {
      Authorization: req.session.token,
    });
    res.end(JSON.stringify(result.data.genres));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}