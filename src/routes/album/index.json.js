import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const AlbumsQuery = gql`
query Albums {
  albums {
   total
   albums {
    id
    name
    coverUrl
    artists {
      id
      name
      coverUrl
    }
    songs {
      id
      title
      coverUrl
    }   
   }
  }
}
`;
export async function get(req, res) {
  try {
    var result = await graphQLClient.request(AlbumsQuery, {}, {
      Authorization: req.session.token,
    });
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    res.end(JSON.stringify(result.albums));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}