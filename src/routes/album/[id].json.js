import {
  gql
} from '@apollo/client/core';
import graphQLClient from "../../graphql_client";

const AlbumQuery = gql`
query Album($id: String!) {
  album(id: $id) {
    id
    name
    coverUrl
    artists {
      id
      name
      coverUrl
    }
    songs {
      id
      title
      duration
      coverUrl
      signedUrl {
        signedUrl
      }
    }   
  }
}
`;
export async function get(req, res, next) {
  try {
    const { id } = req.params;
    var result = await graphQLClient.query({
      query: AlbumQuery,
      variables: { id },
      context: {
        headers: {
          Authorization: req.session.token,
        }
      }
    });
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    res.end(JSON.stringify(result.data.album));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}