import * as sapper from '@sapper/app';
import "./main.scss";

sapper.start({
	target: document.querySelector('#sapper')
});