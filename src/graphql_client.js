import {
    ApolloClient,
    createHttpLink,
    InMemoryCache
} from '@apollo/client/core';
import fetch from 'cross-fetch';

const { API_URL } = process.env;
const GRAPHQL_ENDPOINT = API_URL || "http://localhost:8081/query";

const graphQLClient = new ApolloClient({
    ssrMode: true,
    link: createHttpLink({
        uri: GRAPHQL_ENDPOINT,
        fetch,
        credentials: 'include',
        headers: {
            "User-Agent": "elecktik_svelte",
        }
    }),
    ssrForceFetchDelay: 100,
    cache: new InMemoryCache(),
});



export default graphQLClient;