import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import * as sapper from '@sapper/server';
import { json } from 'body-parser';
import * as Sentry from '@sentry/node';
import * as Tracing from '@sentry/tracing';
const redis = require('redis')
const session = require('express-session')
let RedisStore = require('connect-redis')(session)

const { PORT, NODE_ENV, SESSION_SECRET, REDIS_URL, SENTRY_DSN, APP_VERSION } = process.env;
const dev = NODE_ENV === 'development';
const HEIGHT_HOUR_EXPIRATION = 28800000;
const redisurl = REDIS_URL !== ''

let redisClient = redis.createClient({ url: redisurl ? REDIS_URL : undefined });
const app = polka()

Sentry.init({
	dsn: SENTRY_DSN,
	integrations: [
		// enable HTTP calls tracing
		new Sentry.Integrations.Http({ tracing: true }),
		// enable Express.js middleware tracing
		new Tracing.Integrations.Express({
			// to trace all requests to the default router
			app,
		}),
	],
	tracesSampleRate: 0.6,
});

app.use(
	json({
		limit: '1mb'
	}),
	session({
		secret: SESSION_SECRET || 'SomeSecretStringThatIsNotInGithub',
		resave: true,
		saveUninitialized: true,
		cookie: {
			secure: false,
			sameSite: false,
			maxAge: HEIGHT_HOUR_EXPIRATION,
		},
		store: new RedisStore({ client: redisClient }),
	}),
	compression({ threshold: 0 }),
	sirv('static', { dev }),
	sapper.middleware({
		session: (req, _) => ({
			version: APP_VERSION,
			token: req.session.token,
			user: req.session.user,
			songs: req.session.songs,
			playlistId: req.session.playlistId,
			playlistName: req.session.playlistName,
			currentIndex: req.session.currentIndex,
			settings: req.session.settings,
		})
	})
);
// RequestHandler creates a separate execution context using domains, so that every
// transaction/span/breadcrumb is attached to its own Hub instance
app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());
// ErrorHandler catch error
app.use(Sentry.Handlers.errorHandler());


app.listen(PORT, err => {
	if (err) console.log('error', err);
	console.log(`\n🚀 Running v-${APP_VERSION} on '${process.arch}'-'${process.platform}', listening on port [${PORT}]\n`);
});

