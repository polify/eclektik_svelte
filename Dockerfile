FROM node:alpine as build
WORKDIR /build
COPY package.json yarn.lock ./
RUN yarn install --ignore-optional
COPY . .
RUN yarn build
FROM node:alpine
WORKDIR /app
COPY --from=build /build/node_modules node_modules
COPY --from=build /build/__sapper__ __sapper__
COPY --from=build /build/static static

CMD [ "/usr/local/bin/node", "/app/__sapper__/build" ]