#!/usr/bin/env bash
source ".$stage.env"
export BUILDAH_FORMAT=docker
echo "[INFO] Will deploy heroku app '$HEROKU_APP_NAME' environment with .$stage.env values"
npx heroku container:login
docker build -t "registry.heroku.com/$HEROKU_APP_NAME/web:latest" . &&\
    docker push "registry.heroku.com/$HEROKU_APP_NAME/web:latest" &&\
    npx heroku container:release web -a $HEROKU_APP_NAME